package com.example.apppqrs.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.apppqrs.MainCoroutineRule
import com.example.apppqrs.core.Result
import com.example.apppqrs.data.Constant
import com.example.apppqrs.data.model.Pqrds
import com.example.apppqrs.domain.GetForm
import com.example.apppqrs.domain.SaveForm
import com.example.apppqrs.domain.SendPqrds
import com.example.apppqrs.getOrAwaitValue
import com.example.apppqrs.mock_data.MockData
import com.example.apppqrs.ui.pqrds.PqrdsUiModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.given
import java.lang.Exception

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner.Silent::class)
class PqrdsViewModelTest {

    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @get:Rule
    val instantTaskExecutorRule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var getForm: GetForm

    @Mock
    lateinit var saveForm: SaveForm

    @Mock
    lateinit var sendPqrds: SendPqrds

    @Mock
    lateinit var observerUi: Observer<PqrdsUiModel>

    @Mock
    lateinit var observerPqrds: Observer<Pqrds>

    lateinit var viewModel: PqrdsViewModel

    @Before
    fun setup() {
        viewModel = PqrdsViewModel(getForm, saveForm, sendPqrds)
        viewModel.uiModel.observeForever(observerUi)
        viewModel.request.observeForever(observerPqrds)
    }

    @After
    fun tearDown() {
        viewModel.uiModel.removeObserver(observerUi)
        viewModel.request.removeObserver(observerPqrds)
    }

    @Test
    fun getFormSuccess() = mainCoroutineRule.runBlockingTest {


        given(getForm.execute()).willReturn(Result.Success(MockData.pqrds))

        viewModel.getForm()

        viewModel.request.getOrAwaitValue(afterObserve = {
            if (it != null) {
                assertEquals(MockData.pqrds, it)
            }

        })


    }


    @Test
    fun getFormFailure() = mainCoroutineRule.runBlockingTest {

        given(getForm.execute()).willReturn(Result.Failure(Exception(Constant.FAILURE_MSG)))

        viewModel.getForm()

        viewModel.uiModel.getOrAwaitValue(afterObserve = {
            if (it?.showMessageDialog == Constant.FAILURE_MSG) {
                assertEquals(Constant.FAILURE_MSG, it.showMessageDialog)
            }

        })

    }


    @Test
    fun validateGetFormSuccess() = mainCoroutineRule.runBlockingTest {

        val result = Result.Success(MockData.pqrds)

        viewModel.validateGetForm(result)

        viewModel.request.getOrAwaitValue(afterObserve = {
            if (it != null) {
                assertEquals(MockData.pqrds, it)
            }

        })

    }


    @Test
    fun validateGetFormFailure() {

        val result = Result.Failure<Pqrds>(Exception(Constant.FAILURE_MSG))

        viewModel.validateGetForm(result)

        viewModel.uiModel.getOrAwaitValue(afterObserve = {
            if (it != null) {
                assertEquals(result.exception.message, it.showMessageDialog)
            }

        })

    }


    @Test
    fun validateSaveFormSuccess() {

        val result = Result.Success(Constant.SUCCESS_MSJ)

        viewModel.validateSaveForm(result)

        viewModel.uiModel.getOrAwaitValue(afterObserve = {
            if (it != null) {
                assertEquals(result.data, it.showMessageDialog)
            }

        })
    }

    @Test
    fun validateSaveFormFailure() {

        val result = Result.Failure<String>(Exception(Constant.SUCCESS_MSJ))

        viewModel.validateSaveForm(result)

        viewModel.uiModel.getOrAwaitValue(afterObserve = {
            if (it != null) {
                assertEquals(result.exception.message, it.showMessageDialog)
            }

        })

    }

    @Test
    fun saveFormSuccess() = mainCoroutineRule.runBlockingTest {


        val expected = Result.Success(Constant.SUCCESS_MSJ)
        given(saveForm.execute(MockData.pqrds)).willReturn(expected)

        viewModel.saveForm(MockData.pqrds)

        viewModel.uiModel.getOrAwaitValue(afterObserve = {
            if (it?.showMessageDialog == Constant.SUCCESS_MSJ) {
                assertEquals(Constant.SUCCESS_MSJ, it.showMessageDialog)
            }

        })


    }


    @Test
    fun saveFormFailure() = mainCoroutineRule.runBlockingTest {

        val expected = Result.Failure<String>(Exception(Constant.FAILURE_MSG))
        given(saveForm.execute(MockData.pqrds)).willReturn(expected)

        viewModel.saveForm(MockData.pqrds)

        viewModel.uiModel.getOrAwaitValue(afterObserve = {
            if (it?.showMessageDialog == Constant.FAILURE_MSG) {
                assertEquals(Constant.FAILURE_MSG, it.showMessageDialog)
            }

        })


    }

    @Test
    fun sendPqrdsSuccess() = mainCoroutineRule.runBlockingTest {

        val expected = Result.Success(Constant.SUCCESS_MSJ)
        given(sendPqrds.execute(MockData.pqrds)).willReturn(expected)

        viewModel.sendPqrds(MockData.pqrds)

        viewModel.uiModel.getOrAwaitValue(afterObserve = {
            if (it?.showMessageDialog == Constant.SUCCESS_MSJ) {
                assertEquals(Constant.SUCCESS_MSJ, it.showMessageDialog)
            }

        })

    }

    @Test
    fun sendPqrdsFailure() = mainCoroutineRule.runBlockingTest {

        val expected = Result.Failure<String>(Exception(Constant.FAILURE_MSG))
        given(sendPqrds.execute(MockData.pqrds)).willReturn(expected)

        viewModel.sendPqrds(MockData.pqrds)

        viewModel.uiModel.getOrAwaitValue(afterObserve = {
            if (it?.showMessageDialog == Constant.FAILURE_MSG) {
                assertEquals(Constant.FAILURE_MSG, it.showMessageDialog)
            }

        })

    }


    @Test
    fun validateSendRequestsSuccess() {

        val expected = Result.Success(Constant.SUCCESS_MSJ)

        viewModel.validateSendRequests(expected)

        viewModel.uiModel.getOrAwaitValue(afterObserve = {
            if (it?.showMessageDialog == Constant.SUCCESS_MSJ) {
                assertEquals(Constant.SUCCESS_MSJ, it.showMessageDialog)
            }

        })

    }


    @Test
    fun validateSendRequestsFailure() {

        val expected = Result.Failure<String>(Exception(Constant.FAILURE_MSG))

        viewModel.validateSendRequests(expected)

        viewModel.uiModel.getOrAwaitValue(afterObserve = {
            if (it?.showMessageDialog == Constant.FAILURE_MSG) {
                assertEquals(Constant.FAILURE_MSG, it.showMessageDialog)
            }

        })

    }

}