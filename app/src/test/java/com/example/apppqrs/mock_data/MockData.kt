package com.example.apppqrs.mock_data

import com.example.apppqrs.data.model.InfoPerson
import com.example.apppqrs.data.model.Pqrds

object MockData {


    val pqrds = Pqrds(
        isAnonymous = 0,
        infoPerson = InfoPerson(
            typePerson = "Juridica"
        )
    )

}