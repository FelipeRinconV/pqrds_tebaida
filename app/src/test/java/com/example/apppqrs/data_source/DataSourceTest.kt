package com.example.apppqrs.data_source

import com.example.apppqrs.core.Result
import com.example.apppqrs.data.datasource.DataSource
import com.example.apppqrs.data.preference.PreferenceRequest
import com.example.apppqrs.mock_data.MockData
import junit.framework.TestCase.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.given


@RunWith(MockitoJUnitRunner::class)
class DataSourceTest {

    @Mock
    private lateinit var preferences: PreferenceRequest

    private lateinit var dataSource: DataSource

    @Before
    fun setup() {
        dataSource = DataSource(preferences)
    }

    @Test
    fun getFormSuccess() {

        val expected = Result.Success(MockData.pqrds)

        given(preferences.request).willReturn(MockData.pqrds)

        val result = dataSource.getForm()

        assertEquals(result, expected)

    }


}