package com.example.apppqrs.domain

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.apppqrs.MainCoroutineRule
import com.example.apppqrs.core.Result
import com.example.apppqrs.data.Constant
import com.example.apppqrs.data.model.Pqrds
import com.example.apppqrs.data.repository.RequestRepository
import junit.framework.TestCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.any
import org.mockito.kotlin.given
import java.lang.Exception


@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class SendPqrdsTest {


    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @get: Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    lateinit var requestRepository: RequestRepository

    private lateinit var sendPqrds: SendPqrds

    @Before
    fun setup() {
        sendPqrds = SendPqrds(requestRepository)
    }

    @Test
    fun executeSuccess() = mainCoroutineRule.runBlockingTest {
        val expected = Result.Success(Constant.SUCCESS_MSJ)
        //given

        given(sendPqrds.execute(any())).willReturn(expected)

        //when
        val result = sendPqrds.execute(Pqrds())

        TestCase.assertEquals(expected, result)


    }

    @Test
    fun executeFailure() = mainCoroutineRule.runBlockingTest {
        val expected = Result.Failure<String>(Exception(Constant.FAILURE_MSG))
        //given

        given(sendPqrds.execute(any())).willReturn(expected)

        //when
        val result = sendPqrds.execute(Pqrds())

        TestCase.assertEquals(expected, result)


    }
}