package com.example.apppqrs.domain

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.apppqrs.MainCoroutineRule
import com.example.apppqrs.core.Result
import com.example.apppqrs.data.Constant
import com.example.apppqrs.data.model.Pqrds
import com.example.apppqrs.data.repository.RequestRepository
import com.example.apppqrs.mock_data.MockData
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.any
import org.mockito.kotlin.given
import java.lang.Exception


@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class SaveFormTest {

    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @get: Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    lateinit var requestRepository: RequestRepository

    private lateinit var saveForm: SaveForm

    @Before
    fun setup() {
        saveForm = SaveForm(requestRepository)
    }

    @Test
    fun executeSuccess() = mainCoroutineRule.runBlockingTest {
        val expected = Result.Success(Constant.SUCCESS_MSJ)
        //given

        given(saveForm.execute(any())).willReturn(expected)

        //when
        val result = saveForm.execute(Pqrds())

        assertEquals(expected, result)


    }

    @Test
    fun executeFailure() = mainCoroutineRule.runBlockingTest {
        val expected = Result.Failure<String>(Exception(Constant.FAILURE_MSG))
        //given

        given(saveForm.execute(any())).willReturn(expected)

        //when
        val result = saveForm.execute(Pqrds())

        assertEquals(expected, result)


    }


}