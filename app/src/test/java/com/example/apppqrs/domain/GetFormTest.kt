package com.example.apppqrs.domain

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.apppqrs.MainCoroutineRule
import com.example.apppqrs.core.Result
import com.example.apppqrs.data.Constant
import com.example.apppqrs.data.model.Pqrds
import com.example.apppqrs.data.repository.RequestRepository
import com.example.apppqrs.mock_data.MockData
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.lang.Exception


@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class GetFormTest {


    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @get: Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    lateinit var requestRepository: RequestRepository

    private lateinit var getForm: GetForm

    @Before
    fun setup() {
        getForm = GetForm(requestRepository)
    }

    @Test
    fun executeSuccess() = mainCoroutineRule.runBlockingTest {

        val expected = Result.Success(MockData.pqrds)
        //given
        BDDMockito.given(requestRepository.getForm()).willReturn(expected)
        //when
        val actual = getForm.execute()
        //then
        assertEquals(expected, actual)
    }


    @Test
    fun executeFailure() = mainCoroutineRule.runBlockingTest {

        val expected = Result.Failure<Pqrds>(Exception(Constant.FAILURE_MSG))
        //given
        BDDMockito.given(requestRepository.getForm()).willReturn(expected)
        //when
        val actual = getForm.execute()
        //then
        assertEquals(expected, actual)

    }

}