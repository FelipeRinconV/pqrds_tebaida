package com.example.apppqrs.repository

import com.example.apppqrs.core.Result
import com.example.apppqrs.data.Constant
import com.example.apppqrs.data.datasource.DataSource
import com.example.apppqrs.data.model.Pqrds
import com.example.apppqrs.data.repository.RequestImp
import com.example.apppqrs.data.repository.RequestRepository
import com.example.apppqrs.mock_data.MockData
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.any
import org.mockito.kotlin.given
import java.lang.Exception


@RunWith(MockitoJUnitRunner::class)
class RequestRepositoryTest {


    @Mock
    private lateinit var dataSource: DataSource

    private lateinit var requestRepository: RequestRepository

    @Before
    fun setup() {
        requestRepository = RequestImp(dataSource)
    }

    @Test
    fun getFormSuccess() = runBlockingTest {

        val expected = Result.Success(MockData.pqrds)
        given(dataSource.getForm()).willReturn(expected)

        val result = requestRepository.getForm()

        assertEquals(expected, result)
    }

    @Test
    fun getFormFailure() = runBlockingTest {
        val expected = Result.Failure<Pqrds>(Exception(Constant.FAILURE_MSG))
        given(dataSource.getForm()).willReturn(expected)

        val result = requestRepository.getForm()

        assertEquals(expected, result)

    }


    @Test
    fun saveFormSuccess() = runBlockingTest {

        val expected = Result.Success(Constant.SUCCESS_MSJ)
        given(dataSource.saveForm(any())).willReturn(expected)

        val result = requestRepository.saveForm(Pqrds())

        assertEquals(expected, result)

    }


    @Test
    fun saveFormFailure() = runBlockingTest {

        val expected = Result.Failure<String>(Exception(Constant.FAILURE_MSG))
        given(dataSource.saveForm(any())).willReturn(expected)

        val result = requestRepository.saveForm(Pqrds())

        assertEquals(expected, result)

    }

    @Test
    fun sendRequestSuccess() = runBlockingTest {

        val expected = Result.Success(Constant.SUCCESS_MSJ)
        given(dataSource.sendRequest(any())).willReturn(expected)

        val result = requestRepository.sendRequest(Pqrds())

        assertEquals(expected, result)

    }


    @Test
    fun sendRequestFailure() = runBlockingTest {

        val expected = Result.Failure<String>(Exception(Constant.FAILURE_MSG))
        given(dataSource.sendRequest(any())).willReturn(expected)

        val result = requestRepository.sendRequest(Pqrds())

        assertEquals(expected, result)

    }

}