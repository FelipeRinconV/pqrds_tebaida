package com.example.apppqrs.di

import com.example.apppqrs.data.repository.RequestImp
import com.example.apppqrs.data.repository.RequestRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent


@Module
@InstallIn(ActivityRetainedComponent::class)
abstract class RequestServiceModule {

    @Binds
    abstract fun bindsRequestRepository(requestImp: RequestImp): RequestRepository

}