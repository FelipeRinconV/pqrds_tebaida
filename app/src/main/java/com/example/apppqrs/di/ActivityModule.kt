package com.example.apppqrs.di

import android.content.Context
import com.example.apppqrs.data.datasource.DataSource
import com.example.apppqrs.data.preference.PreferenceRequest
import com.example.apppqrs.data.repository.RequestRepository
import com.example.apppqrs.domain.GetForm
import com.example.apppqrs.domain.SaveForm
import com.example.apppqrs.domain.SendPqrds
import com.example.apppqrs.ui.PqrdsViewModel
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.qualifiers.ApplicationContext


@Module
@InstallIn(ActivityRetainedComponent::class)
class ActivityModule {

    @Provides
    fun createGetForm(repository: RequestRepository): GetForm = GetForm(repository)

    @Provides
    fun createSaveForm(repository: RequestRepository): SaveForm = SaveForm(repository)

    @Provides
    fun createRequestViewModel(
        getForm: GetForm,
        saveForm: SaveForm,
        sendPqrds: SendPqrds
    ): PqrdsViewModel = PqrdsViewModel(getForm, saveForm, sendPqrds)

    @Provides
    fun createDataSource(preferences: PreferenceRequest): DataSource = DataSource(preferences)


    @Provides
    fun createPreference(@ApplicationContext context: Context): PreferenceRequest =
        PreferenceRequest(context)

    @Provides
    fun createMoshi(): Moshi = Moshi.Builder().build()

}