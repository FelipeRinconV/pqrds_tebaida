package com.example.apppqrs

import android.app.Application
import android.content.Context
import com.example.apppqrs.data.preference.PreferenceRequest
import dagger.Module
import dagger.Provides
import dagger.hilt.android.HiltAndroidApp
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@HiltAndroidApp
class MainApplication : Application()