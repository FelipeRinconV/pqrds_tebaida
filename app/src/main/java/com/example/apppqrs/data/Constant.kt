package com.example.apppqrs.data

object Constant {

    val SUCCESS_MSJ: String = "Success"
    val FAILURE_MSG: String = "Failure"
    val TEXT_EMAIL_ADDRESS = 33
    val DATOS_ENVIADOS: String = "Datos enviados exitosamente"
    val REQUEST: String = "request"
    val PQRDS_PREFERENCE_KEY = "pqrds"
    val REQUEST_FILE = 1


}