package com.example.apppqrs.data.repository


import android.net.Uri
import com.example.apppqrs.core.Result
import com.example.apppqrs.data.datasource.DataSource
import com.example.apppqrs.data.model.Pqrds
import dagger.hilt.android.scopes.ActivityRetainedScoped
import javax.inject.Inject

@ActivityRetainedScoped
class RequestImp @Inject constructor(
    private val dataSource: DataSource
) : RequestRepository {

    override suspend fun getForm(): Result<Pqrds> = dataSource.getForm()

    override suspend fun saveForm(request: Pqrds): Result<String> = dataSource.saveForm(request)

    override suspend fun sendRequest(request: Pqrds): Result<String> =
        dataSource.sendRequest(request)


}