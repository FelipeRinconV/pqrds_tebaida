package com.example.apppqrs.data.model

import com.squareup.moshi.JsonClass
import java.io.Serializable
import java.util.*

@JsonClass(generateAdapter = true)
data class Pqrds(
    var isAnonymous: Int = -1,
    var infoPerson: InfoPerson = InfoPerson(),
    var infoContact: InfoContact = InfoContact(),
    var infoRequest: RequestInfo = RequestInfo()
) : Serializable

@JsonClass(generateAdapter = true)
data class InfoPerson(
    var typePerson: String = "",
    var firstName: String = "",
    var secondName: String = "",
    var surName: String = "",
    var secondSurName: String = "",
    var typeDocument: String = "",
    var numDocument: String = "",
    var gender: String = ""
) : Serializable

@JsonClass(generateAdapter = true)
data class InfoContact(
    var country: String = "",
    var address: String = "",
    var numPhoneMobile: String = "",
    var landLine: String = "",
    var email: String = ""
) : Serializable


@JsonClass(generateAdapter = true)
data class RequestInfo(
    var date: String = "",
    var description: String = "",
    var urlPathFile: String = "",
    var numContact: String = "",
    var indicativeNum: String = "",
    var emailContact: String = ""
) : Serializable






