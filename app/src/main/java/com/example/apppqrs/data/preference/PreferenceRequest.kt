package com.example.apppqrs.data.preference

import android.content.Context
import com.example.apppqrs.data.Constant
import com.example.apppqrs.data.model.Pqrds
import hu.autsoft.krate.SimpleKrate
import hu.autsoft.krate.moshi.moshiPref

class PreferenceRequest(context: Context) : SimpleKrate(context) {
    var request: Pqrds by moshiPref(Constant.PQRDS_PREFERENCE_KEY, Pqrds())
}
