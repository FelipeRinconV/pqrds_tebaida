package com.example.apppqrs.data.repository

import android.net.Uri
import com.example.apppqrs.core.Result
import com.example.apppqrs.data.model.Pqrds

interface RequestRepository {

    suspend fun getForm(): Result<Pqrds>

    suspend fun saveForm(request: Pqrds): Result<String>

    suspend fun sendRequest(request: Pqrds): Result<String>



}