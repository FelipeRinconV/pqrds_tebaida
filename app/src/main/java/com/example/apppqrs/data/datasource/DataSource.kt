package com.example.apppqrs.data.datasource

import android.net.Uri
import com.example.apppqrs.core.Result
import com.example.apppqrs.data.Constant
import com.example.apppqrs.data.model.Pqrds
import com.example.apppqrs.data.preference.PreferenceRequest
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import dagger.hilt.android.scopes.ActivityRetainedScoped
import kotlinx.coroutines.tasks.await
import java.util.*
import javax.inject.Inject


@ActivityRetainedScoped
class DataSource @Inject constructor(
    private val preferences: PreferenceRequest
) {

    suspend fun sendRequest(request: Pqrds): Result<String> {
        return try {
            if (request.infoRequest.urlPathFile.isNotBlank()) {
                var uriFile = Uri.parse(request.infoRequest.urlPathFile)

                if (uriFile != null) {
                    val randomName = UUID.randomUUID().toString()
                    val ref = FirebaseStorage.getInstance().reference.child("files/$randomName")
                    val urlFile =
                        ref.putFile(uriFile).await().storage.downloadUrl.await().toString()
                    request.infoRequest.urlPathFile = urlFile
                }
            }
            FirebaseFirestore.getInstance().collection("pqrds").add(request)
            Result.Success(Constant.DATOS_ENVIADOS)
        } catch (e: Exception) {
            Result.Failure(e)
        }

    }

    fun getForm(): Result<Pqrds> {
        return try {
            Result.Success(preferences.request)
        } catch (e: Exception) {
            Result.Failure(e)
        }

    }

    fun saveForm(request: Pqrds): Result<String> {
        return try {
            preferences.request = request
            return Result.Success("Datos almacenados correctamente")
        } catch (e: Exception) {
            Result.Failure(e)
        }

    }


}