package com.example.apppqrs.ui.pqrds

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.example.apppqrs.R
import com.example.apppqrs.data.Constant
import com.example.apppqrs.data.model.InfoContact
import com.example.apppqrs.data.model.InfoPerson
import com.example.apppqrs.data.model.Pqrds
import com.example.apppqrs.databinding.FragmentMainMenuFragmentBinding
import com.example.apppqrs.ui.MainActivity
import com.example.apppqrs.ui.PqrdsViewModel
import com.example.awesomedialog.*
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.*

@AndroidEntryPoint
class MainMenuFragment : Fragment() {

    private lateinit var binding: FragmentMainMenuFragmentBinding

    private val viewModel: PqrdsViewModel by activityViewModels()
    private val uiModel = Observer<PqrdsUiModel> { handleUi(it) }
    private val requestObserver = Observer<Pqrds> { handleRequest(it) }
    private lateinit var request: Pqrds


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        configureViewBinding(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        startObserve()
        configureMenu()
        setupListener()
        viewModel.getForm()
    }


    private fun startObserve() {
        viewModel.request.observe(viewLifecycleOwner, requestObserver)
        viewModel.uiModel.observe(viewLifecycleOwner, uiModel)
    }

    private fun configureViewBinding(inflater: LayoutInflater) {
        binding = FragmentMainMenuFragmentBinding.inflate(inflater)
    }


    private fun setupListener() {

        binding.apply {


            txtCompleteRequestInfo.setOnClickListener {
                goInfoRequestFragment()
            }
            btnGoCompleteRequest.setOnClickListener {
                goInfoRequestFragment()
            }


            txtCompleteContactInfo.setOnClickListener {
                goInfoContactFragment()
            }
            btnGoContactInfo.setOnClickListener {
                goInfoContactFragment()
            }


            txtCompletePersonInfo.setOnClickListener {
                goInfoPersonFragment()
            }
            btnGoPersonInfo.setOnClickListener {
                goInfoPersonFragment()
            }

            rdBtnYes.setOnCheckedChangeListener { _, isChecked ->
                changeVisibility(layoutPersonalInfo, !isChecked)
                request.isAnonymous = if (isChecked) 1 else 0
                viewModel.saveForm(request)
            }

            rdBtnNot.setOnCheckedChangeListener { _, isChecked ->
                changeVisibility(layoutPersonalInfo, isChecked)
                request.isAnonymous = if (isChecked) 0 else 1
                viewModel.saveForm(request)
            }

            btnSendRequest.setOnClickListener {

                val msjValidation = validateData()

                if (msjValidation.isNotBlank()) {
                    showDialogError(msjValidation)
                } else {
                    confirmSendData()
                }

            }
        }
    }


    private fun validateData(): String {

        if (request.isAnonymous == 0 && !infoPersonIsCompleted(request.infoPerson)) {
            return getStringById(R.string.error_ingrese_info_person)
        }

        if (!infoContactIsCompleted(request.infoContact)) {
            return getStringById(R.string.error_ingrese_info_contact)
        }

        if (!infoRequestIsCompleted()) {
            return getStringById(R.string.error_ingrese_info_pqrds)
        }

        return ""
    }


    private fun confirmSendData() {
        AwesomeDialog.build(requireActivity())
            .title(
                "Envio de datos",
                titleColor = ContextCompat.getColor(
                    requireContext(),
                    android.R.color.white
                )
            )
            .body(
                "¿Confirma el envio de los datos?",
                color = ContextCompat.getColor(requireContext(), android.R.color.white)
            )
            .icon(R.drawable.ic_baseline_send_24)
            .background(R.color.green_700)
            .onPositive(
                "Confirmar",
            ) {
                if (binding.rdBtnYes.isChecked) {
                    request.infoPerson = InfoPerson()
                }

                val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                val date = Date()

                request.infoRequest.date = dateFormat.format(date)
                viewModel.sendPqrds(request)
            }
            .onNegative("Cancelar")


    }

    private fun goInfoRequestFragment() {
        val bundleRequest = bundleOf(Constant.REQUEST to request)
        Navigation.findNavController(requireView())
            .navigate(R.id.action_mainMenuFragment_to_infoPqrsFragment, bundleRequest)
    }

    private fun goInfoPersonFragment() {
        val bundleRequest = bundleOf(Constant.REQUEST to request)
        Navigation.findNavController(requireView())
            .navigate(R.id.action_mainMenuFragment_to_infoPersonFragment, bundleRequest)
    }

    private fun goInfoContactFragment() {
        val bundleRequest = bundleOf(Constant.REQUEST to request)
        Navigation.findNavController(requireView())
            .navigate(R.id.action_mainMenuFragment_to_infocContactFragment, bundleRequest)
    }

    private fun configureMenu() {
        (activity as MainActivity?)?.apply {
            changeTitle("Alcaldía de tebaida")
        }
    }

    private fun changeVisibility(view: View, visibility: Boolean) {
        view.isVisible = visibility
    }

    private fun handleUi(model: PqrdsUiModel) {

        model.apply {
            if (showProgress) showProgress(View.VISIBLE)
            else showProgress(View.GONE)
            if (showMessageDialog.isNotBlank() && showMessageDialog == Constant.DATOS_ENVIADOS) {

                showDialogSuccess(showMessageDialog)

            }
        }

    }

    private fun showDialogError(showMessageDialog: String) {
        AwesomeDialog.build(requireActivity())
            .title("Aviso")
            .body(showMessageDialog)
            //.icon(R.drawable.ic_dialog_info)
            .onPositive("Aceptar")
    }

    private fun showDialogSuccess(showMessageDialog: String) {
        AwesomeDialog.build(requireActivity())
            .title(
                "Enviado",
                titleColor = ContextCompat.getColor(requireContext(), android.R.color.white)
            )
            .body(
                showMessageDialog,
                color = ContextCompat.getColor(requireContext(), android.R.color.white)
            )
            .icon(R.drawable.ic_congrts)
            .background(R.color.green_700)
            .onPositive(
                "Aceptar",
            ) {
                viewModel.saveForm(Pqrds())
                request = Pqrds()
                initUi()
            }
    }


    private fun initUi() {
        binding.apply {
            request.apply {
                if (isAnonymous == 1) {
                    layoutPersonalInfo.visibility = View.GONE
                    rdBtnYes.isChecked = true
                } else if (isAnonymous == 0) {
                    layoutPersonalInfo.visibility = View.VISIBLE
                    rdBtnNot.isChecked = true
                }

                configureUiInfoPerson()
                configureUiInfoContact()
                configureUiInfoRequest()

            }
        }
    }

    private fun handleRequest(it: Pqrds) {
        this.request = it
        initUi()
    }

    private fun configureUiInfoRequest() {
        binding.apply {
            request.apply {

                if (infoRequestIsCompleted()) {
                    txtCompleteRequestInfo.visibility = View.GONE
                    txtCompletedMenuInforRequest.visibility = View.VISIBLE
                    if (request.infoRequest.urlPathFile.isBlank()) {
                        txtUrlFile.text = "Sin archivo"
                    }
                } else {
                    txtCompleteRequestInfo.visibility = View.VISIBLE
                    txtCompletedMenuInforRequest.visibility = View.GONE
                }
                if (request.infoRequest.urlPathFile.isBlank()) {
                    txtUrlFile.text = ""
                } else {
                    txtUrlFile.text = infoRequest.urlPathFile
                }

                txtMenuDescription.text = infoRequest.description
                txtContactMobile.text = infoRequest.numContact
                txtContactEmail.text = infoRequest.emailContact

            }
        }
    }

    private fun configureUiInfoContact() {
        binding.apply {
            request.apply {

                if (infoContactIsCompleted(infoContact)) {
                    txtCompleteContactInfo.visibility = View.GONE
                    txtCompletedContactInfo.visibility = View.VISIBLE
                } else {
                    txtCompleteContactInfo.visibility = View.VISIBLE
                    txtCompletedContactInfo.visibility = View.GONE
                }

                txtMenuCountry.text = infoContact.country
                txtMenuHomeAddress.text = infoContact.address
                txtMenuLandLine.text = infoContact.landLine
                txtMenuMobilePhone.text = infoContact.numPhoneMobile
                txtMenuEmail.text = infoContact.email


            }
        }

    }

    private fun configureUiInfoPerson() {
        binding.apply {


            request.apply {
                if (infoPersonIsCompleted(infoPerson)) {
                    txtCompletePersonInfo.visibility = View.GONE
                    txtInfoPersonCompleted.visibility = View.VISIBLE
                } else {
                    txtCompletePersonInfo.visibility = View.VISIBLE
                    txtInfoPersonCompleted.visibility = View.GONE
                }


                if (infoPerson.typePerson.isNotBlank() && infoPerson.gender.isNotBlank()) {
                    txtMenuTypePerson.text = infoPerson.typePerson + ", " + infoPerson.gender
                } else {
                    txtMenuTypePerson.text = infoPerson.typePerson + " " + infoPerson.gender
                }

                txtMenuName.text = infoPerson.firstName

                txtMenuName.text = infoPerson.firstName + " " + infoPerson.secondName
                txtMenuLastName.text = infoPerson.surName

                txtMenuLastName.text = infoPerson.surName + " " + infoPerson.secondSurName

                txtMenuNumDocument.text = infoPerson.typeDocument + " " + infoPerson.numDocument
            }
        }

    }

    private fun infoPersonIsCompleted(infoPerson: InfoPerson): Boolean {

        infoPerson.apply {

            return typePerson.isNotBlank() && firstName.isNotBlank() && secondName.isNotBlank() &&
                    surName.isNotBlank() && secondSurName.isNotBlank() && typeDocument.isNotBlank()
                    && numDocument.isNotBlank() && gender.isNotBlank()


        }

    }


    private fun infoContactIsCompleted(infoContact: InfoContact): Boolean {

        infoContact.apply {

            return country.isNotBlank() && address.isNotBlank() &&
                    landLine.isNotBlank() && numPhoneMobile.isNotBlank()
                    && email.isNotBlank()

        }

    }


    private fun infoRequestIsCompleted(): Boolean {

        request.infoRequest.apply {

            return description.isNotBlank() &&
                    (numContact.isNotBlank() || emailContact.isNotBlank())


        }

    }


    private fun showProgress(visibility: Int) {
        binding.apply {
            indeterminateBar.visibility = visibility
        }
    }

    fun getStringById(idString: Int): String {

        return requireContext().resources.getString(idString)
    }

}