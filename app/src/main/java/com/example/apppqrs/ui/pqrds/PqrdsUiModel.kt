package com.example.apppqrs.ui.pqrds

data class PqrdsUiModel(
    val showProgress: Boolean = false,
    val showMessageDialog: String = ""
)