package com.example.apppqrs.ui.pqrds

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.apppqrs.R
import com.example.apppqrs.data.Constant
import com.example.apppqrs.data.model.Pqrds
import com.example.apppqrs.data.model.RequestInfo
import com.example.apppqrs.databinding.FragmentInfoPqrsBinding
import com.example.apppqrs.ui.MainActivity
import com.example.apppqrs.ui.PqrdsViewModel
import com.example.apppqrs.utils.Utils
import com.example.awesomedialog.*


class InfoPqrsFragment : Fragment() {

    private lateinit var binding: FragmentInfoPqrsBinding
    private val viewModel: PqrdsViewModel by activityViewModels()
    private val uiModelObserve = Observer<PqrdsUiModel> { handleUi(it) }
    private lateinit var request: Pqrds


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        configureViewBinding(inflater)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        configureMenu()
        getInfoArg()
        configureListener()
        configureObserver()
        initUi()
    }


    private fun configureViewBinding(inflater: LayoutInflater) {
        binding = FragmentInfoPqrsBinding.inflate(inflater)
    }


    private fun handleUi(uiModel: PqrdsUiModel) {

        uiModel.apply {
            if (showProgress) showProgress(View.VISIBLE)
            else showProgress(View.GONE)
        }

    }


    private fun showProgress(visibility: Int) {
        binding.apply {
            indeterminateBar.visibility = visibility
        }
    }


    private fun showDialogError(showMessageDialog: String) {
        AwesomeDialog.build(requireActivity())
            .title("Aviso")
            .body(showMessageDialog)
            //.icon(R.drawable.ic_dialog_info)
            .onPositive("Aceptar")
    }


    private fun configureObserver() {

        viewModel.uiModel.observe(viewLifecycleOwner, uiModelObserve)
    }

    private fun configureListener() {

        binding.apply {

            btnSave.setOnClickListener {
                saveInfo()
            }

            btnAddDocument.setOnClickListener {
                val intent = Intent(Intent.ACTION_GET_CONTENT).apply {
                    type = "*/*"
                }

                startActivityForResult(intent, Constant.REQUEST_FILE)
            }
        }

        binding.imgDeleteFile.setOnClickListener {

            binding.apply {
                request.infoRequest.urlPathFile = ""
                btnAddDocument.text = "Añadir archivo "


                binding.imgDeleteFile.visibility = View.GONE
            }


        }

    }

    private fun saveInfo() {

        binding.apply {

            validateDescription()
            if (isValidContact(txtEmailContact.text.toString())) {
                showDialogError("Por favor ingrese un correo valido o el numero de telefono con el indicativo")
            } else {
                if (validateEmail()) {
                    saveForm(txtEmailContact.text.toString())
                } else {
                    txtEmailContact.error = "El correo ingresado no tiene un formato valido."
                }
            }

        }
    }


    private fun validateEmail(): Boolean {

        return if (binding.txtEmailContact.text.toString().isNotBlank()) {
            Utils.isEmail(binding.txtEmailContact.text.toString())
        } else {
            true
        }
    }

    private fun validateDescription() {
        if (binding.txtDescription.text.isNullOrEmpty()) {
            binding.txtDescription.error =
                requireContext().resources.getString(R.string.error_message_mandatory_field)
            return
        }
    }

    private fun isValidContact(email: String): Boolean {
        return (email.isBlank() || !Utils.isEmail(email))
                && (binding.txtIndicative.text.isNullOrBlank() || binding.txtNumMobileContact.text.isNullOrBlank())
    }

    private fun saveForm(emailEntered: String) {

        binding.apply {
            val requestInfo = RequestInfo(
                description = txtDescription.text.toString(),
                urlPathFile = request.infoRequest.urlPathFile,
                numContact = txtNumMobileContact.text.toString(),
                emailContact = emailEntered,
                indicativeNum = txtIndicative.text.toString()
            )

            request.infoRequest = requestInfo
            viewModel.saveForm(request)
            findNavController().popBackStack()
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            val fileData: Uri? = data?.data

            if (fileData != null) {
                binding.btnAddDocument.text = fileData.path.toString()
                request.infoRequest.urlPathFile = fileData.toString()
                binding.imgDeleteFile.visibility = View.VISIBLE
            }


        }
    }


    private fun getInfoArg() {
        arguments?.let {
            request = requireArguments().getSerializable(Constant.REQUEST) as Pqrds
        }

    }


    private fun configureMenu() {
        (activity as MainActivity?)?.apply {
            changeTitle("Informacion adicional")
        }
    }


    private fun initUi() {
        request.infoRequest.apply {
            binding.apply {

                if (urlPathFile.isNotBlank()) {
                    btnAddDocument.text = urlPathFile
                } else {
                    btnAddDocument.text = "Añadir archivo"
                }
                txtDescription.setText(description)

                txtEmailContact.setText(emailContact)
                txtNumMobileContact.setText(numContact)
                txtIndicative.setText(indicativeNum)
            }
        }
    }


}