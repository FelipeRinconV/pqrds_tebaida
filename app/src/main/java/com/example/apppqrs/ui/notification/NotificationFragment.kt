package com.example.apppqrs.ui.notification

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.apppqrs.R
import com.example.apppqrs.databinding.FragmentInfoPersonBinding
import com.example.apppqrs.databinding.FragmentMainMenuFragmentBinding
import com.example.apppqrs.databinding.FragmentNotificationBinding


class NotificationFragment : Fragment() {

    private lateinit var binding: FragmentNotificationBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        configureViewBinding(inflater)
        return binding.root
    }

    private fun configureViewBinding(inflater: LayoutInflater) {
        binding = FragmentNotificationBinding.inflate(inflater)
    }



}