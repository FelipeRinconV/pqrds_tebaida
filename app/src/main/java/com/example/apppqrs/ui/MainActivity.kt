package com.example.apppqrs.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.apppqrs.R
import com.example.apppqrs.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        configureViewBinding()
    }

    private fun configureViewBinding() {
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.topAppBar.title = getString(R.string.alcaldia)
    }

    fun changeTitle(title: String) {
        binding.topAppBar.title = title
    }
}