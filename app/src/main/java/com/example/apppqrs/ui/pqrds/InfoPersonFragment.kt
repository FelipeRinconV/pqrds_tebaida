package com.example.apppqrs.ui.pqrds

import android.R
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.apppqrs.data.Constant
import com.example.apppqrs.data.model.InfoPerson
import com.example.apppqrs.data.model.Pqrds
import com.example.apppqrs.databinding.FragmentInfoPersonBinding
import com.example.apppqrs.ui.MainActivity
import com.example.apppqrs.ui.PqrdsViewModel
import com.example.apppqrs.utils.Utils
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class InfoPersonFragment : Fragment() {

    private lateinit var binding: FragmentInfoPersonBinding
    private val viewModel: PqrdsViewModel by activityViewModels()
    private val uiModelObserve = Observer<PqrdsUiModel> { handleUi(it) }
    private lateinit var request: Pqrds

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        container
        configureViewBinding(inflater)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        configureMenu()
        configureAdapter()
        configureListener()
        getInfoArg()
        configureObserver()
        initUi()

    }

    private fun getInfoArg() {
        arguments?.let {
            request = requireArguments().getSerializable(Constant.REQUEST) as Pqrds
        }

    }

    private fun configureObserver() {
        viewModel.uiModel.observe(viewLifecycleOwner, uiModelObserve)
    }

    private fun configureListener() {

        binding.apply {

            btnSave.setOnClickListener {
                updateInfoPersonRequest()

            }

            txtTypePerson.setOnItemClickListener { _, _, position, _ ->
                request.infoPerson.typePerson = txtTypePerson.adapter.getItem(position).toString()
                txtTypePerson.error = null
            }

            txtTypeDocument.setOnItemClickListener { _, _, position, _ ->
                request.infoPerson.typeDocument =
                    txtTypeDocument.adapter.getItem(position).toString()
                txtTypeDocument.error = null
            }

            tvGender.setOnItemClickListener { _, _, position, _ ->
                request.infoPerson.gender = tvGender.adapter.getItem(position).toString()
                tvGender.error = null
            }

        }

    }


    private fun updateInfoPersonRequest() {
        binding.apply {


            if (Utils.validateField(binding.linearPerson, requireContext())) {
                val infoPerson = InfoPerson(
                    request.infoPerson.typePerson,
                    txtFirstName.text.toString(),
                    txtMiddleName.text.toString(),
                    txtSurName.text.toString(),
                    txtSecondLastName.text.toString(),
                    request.infoPerson.typeDocument,
                    txtNumDocument.text.toString(),
                    request.infoPerson.gender
                )

                if (request.infoPerson.typeDocument.isBlank()) {
                    txtTypeDocument.error =
                        requireContext().resources.getString(com.example.apppqrs.R.string.error_message_mandatory_field)
                    return
                }

                if (txtNumDocument.text.toString().isBlank()) {
                    txtNumDocument.error =
                        requireContext().resources.getString(com.example.apppqrs.R.string.error_message_mandatory_field)
                    return
                }

                request.infoPerson = infoPerson

                viewModel.saveForm(request)
                findNavController().popBackStack()
            }

        }

    }


    private fun configureMenu() {
        (activity as MainActivity?)?.apply {
            changeTitle("Informacion personal")
        }
    }

    private fun configureViewBinding(inflater: LayoutInflater) {
        binding = FragmentInfoPersonBinding.inflate(inflater)
    }

    private fun handleUi(model: PqrdsUiModel) {

        model.apply {

            if (showProgress) showProgress(View.VISIBLE)
            else showProgress(View.GONE)

        }

    }

    private fun showProgress(visibility: Int) {
        binding.apply {
            indeterminateBar.visibility = visibility
        }
    }


    private fun initUi() {


        initTypeDocument(request.infoPerson)

        initGender(request.infoPerson)

        binding.apply {
            request.infoPerson.apply {

                txtFirstName.setText(firstName)
                txtMiddleName.setText(secondName)
                txtSurName.setText(surName)
                txtSecondLastName.setText(secondSurName)
                txtNumDocument.setText(numDocument)

            }
        }

    }


    private fun initTypeDocument(infoPerson: InfoPerson) {

        for (i in 0 until binding.txtTypePerson.adapter.count) {
            if (binding.txtTypePerson.adapter.getItem(i).equals(infoPerson.typePerson)) {
                binding.txtTypePerson.setText(infoPerson.typePerson, false)
            }
        }

    }

    private fun initGender(infoPerson: InfoPerson) {
        for (i in 0 until binding.tvGender.adapter.count) {
            if (binding.tvGender.adapter.getItem(i).equals(infoPerson.gender)) {
                binding.tvGender.setText(infoPerson.gender, false)
            }
        }
    }

    private fun getTypeDocumentAdapter() {
        val items = listOf("C.C", "T.I")
        val adapter =
            ArrayAdapter(requireContext(), R.layout.simple_spinner_dropdown_item, items)
        binding.txtTypeDocument.setAdapter(adapter)
    }


    private fun getTypePersonAdapter() {
        val items = listOf("Juridica", "Natural")
        val adapter =
            ArrayAdapter(requireContext(), R.layout.simple_spinner_dropdown_item, items)
        binding.txtTypePerson.setAdapter(adapter)
    }

    private fun getGenderAdapter() {
        val items = listOf("Hombre", "Mujer")
        var adapter =
            ArrayAdapter(requireContext(), R.layout.simple_spinner_dropdown_item, items)
        binding.tvGender.setAdapter(adapter)
    }

    private fun configureAdapter() {
        getTypeDocumentAdapter()
        getTypePersonAdapter()
        getGenderAdapter()
    }


}