package com.example.apppqrs.ui.pqrds

import android.R
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.apppqrs.data.Constant
import com.example.apppqrs.data.model.Pqrds
import com.example.apppqrs.ui.MainActivity
import com.example.apppqrs.databinding.FragmentInfocContactBinding
import com.example.apppqrs.ui.PqrdsViewModel
import com.example.apppqrs.utils.Utils
import com.example.awesomedialog.*

class InfoContactFragment : Fragment() {

    private lateinit var binding: FragmentInfocContactBinding
    private val viewModel: PqrdsViewModel by activityViewModels()
    private val uiModelObserve = Observer<PqrdsUiModel> { handleUi(it) }
    private lateinit var request: Pqrds

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        configureViewBinding(inflater)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        configureMenu()
        getInfoArg()
        configureAdapter()
        configureListener()
        configureObserver()
        initUi()
    }

    private fun configureAdapter() {

        val lisItemsCountry = listOf("Colombia", "Otros paises")
        val adapter =
            ArrayAdapter(requireContext(), R.layout.simple_spinner_dropdown_item, lisItemsCountry)
        binding.atvCountry.setAdapter(adapter)

    }

    private fun configureObserver() {

        viewModel.uiModel.observe(viewLifecycleOwner, uiModelObserve)
    }

    private fun configureListener() {

        binding.apply {

            btnSave.setOnClickListener {
                updateRequest()

            }


            atvCountry.setOnItemClickListener { _, _, position, _ ->
                request.infoContact.country = atvCountry.adapter.getItem(position) as String
                atvCountry.error = null
            }

        }


    }


    private fun updateRequest() {

        binding.apply {
            if (Utils.validateField(binding.linearInfoContact, requireContext())) {
                request.infoContact.address = txtHomeAddress.text.toString()
                request.infoContact.email = txtEmail.text.toString()
                request.infoContact.numPhoneMobile = txtMobilePhone.text.toString()
                request.infoContact.landLine = txtLandLine.text.toString()

                viewModel.saveForm(request)
                findNavController().popBackStack()
            }
        }

    }

    private fun handleUi(uiModel: PqrdsUiModel) {

        uiModel.apply {
            if (showProgress) showProgress(View.VISIBLE)
            else showProgress(View.GONE)
        }

    }


    private fun showProgress(visibility: Int) {
        binding.apply {
            indeterminateBar.visibility = visibility
        }
    }


    private fun getInfoArg() {
        arguments?.let {
            request = requireArguments().getSerializable(Constant.REQUEST) as Pqrds
        }

    }

    private fun initUi() {
        request.infoContact.apply {
            binding.apply {

                for (i in 0 until atvCountry.adapter.count) {
                    if (atvCountry.adapter.getItem(i).equals(country)) {
                        atvCountry.setText(country, false)
                    }
                }

                txtEmail.setText(email)
                txtHomeAddress.setText(address)
                txtLandLine.setText(landLine)
                txtMobilePhone.setText(numPhoneMobile)


            }


        }
    }


    private fun configureMenu() {
        (activity as MainActivity?)?.apply {
            changeTitle("Informacion de contacto")
        }
    }

    private fun configureViewBinding(inflater: LayoutInflater) {
        binding = FragmentInfocContactBinding.inflate(inflater)
    }




}