package com.example.apppqrs.ui

import android.net.Uri
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.apppqrs.core.Result
import com.example.apppqrs.data.model.Pqrds
import com.example.apppqrs.domain.GetForm
import com.example.apppqrs.domain.SaveForm
import com.example.apppqrs.domain.SendPqrds
import com.example.apppqrs.ui.pqrds.PqrdsUiModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class PqrdsViewModel @ViewModelInject constructor(
    private val getForm: GetForm,
    private val saveForm: SaveForm,
    private val sendPqrds: SendPqrds
) : ViewModel() {

    private val _uiModel = MutableLiveData<PqrdsUiModel>()
    val uiModel: LiveData<PqrdsUiModel>
        get() = _uiModel

    private val _request = MutableLiveData<Pqrds>()
    val request: LiveData<Pqrds>
        get() = _request

    fun getForm() {
        emitUiState(showProgress = true)
        viewModelScope.launch(Dispatchers.IO) {
            getForm.execute().also {
                validateGetForm(it)
            }
        }
    }

    fun validateGetForm(result: Result<Pqrds>) {
        when (result) {
            is Result.Success -> {
                emitUiState(showProgress = false)
                viewModelScope.launch(Dispatchers.Main) {
                    result.data.let {
                        _request.value = it
                    }
                }
            }
            is Result.Failure -> {
                emitUiState(showMessageDialog = result.exception.message.toString())
            }
        }
    }


    fun saveForm(pqrds: Pqrds) {
        emitUiState(showProgress = true)
        viewModelScope.launch(Dispatchers.IO) {
            saveForm.execute(pqrds).also {
                validateSaveForm(it)
            }
        }
    }

    fun validateSaveForm(it: Result<String>) {
        when (it) {
            is Result.Success -> {
                emitUiState(showMessageDialog = it.data)
            }

            is Result.Failure -> {
                emitUiState(showMessageDialog = it.exception.message.toString())
            }

        }
    }

    fun sendPqrds(pqrds: Pqrds) {
        emitUiState(showProgress = true)
        viewModelScope.launch(Dispatchers.IO) {
            sendPqrds.execute(pqrds).also {
                validateSendRequests(it)
            }
        }
    }

    fun validateSendRequests(result: Result<String>) {
        when (result) {
            is Result.Success -> {
                emitUiState(showMessageDialog = result.data)
            }

            is Result.Failure -> {
                emitUiState(showMessageDialog = result.exception.message.toString())
            }
        }
    }

    private fun emitUiState(
        showProgress: Boolean = false,
        showMessageDialog: String = ""
    ) {
        viewModelScope.launch(Dispatchers.Main) {
            val uiModel = PqrdsUiModel(
                showProgress,
                showMessageDialog
            )
            _uiModel.value = uiModel
        }
    }
}