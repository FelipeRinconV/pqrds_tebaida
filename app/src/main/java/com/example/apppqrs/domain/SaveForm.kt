package com.example.apppqrs.domain

import com.example.apppqrs.core.Result
import com.example.apppqrs.data.model.Pqrds
import com.example.apppqrs.data.repository.RequestRepository
import dagger.hilt.android.scopes.ActivityRetainedScoped
import javax.inject.Inject


/**
 *Caso de uso que guarda los datos parciales que se hayan
 *ingresados por el usuario
 */
@ActivityRetainedScoped
class SaveForm @Inject constructor(private val requestRepository: RequestRepository) {

    suspend fun execute(pqrds: Pqrds):Result<String> = requestRepository.saveForm(pqrds)


}