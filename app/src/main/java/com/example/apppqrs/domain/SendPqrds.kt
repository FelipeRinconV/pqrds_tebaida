package com.example.apppqrs.domain

import android.net.Uri
import com.example.apppqrs.core.Result
import com.example.apppqrs.data.model.Pqrds
import com.example.apppqrs.data.repository.RequestRepository
import dagger.hilt.android.scopes.ActivityRetainedScoped
import javax.inject.Inject

/**
 * Caso de uso que envia los datos del pqrds a firestore
 */
@ActivityRetainedScoped
class SendPqrds @Inject constructor(private val requestRepository: RequestRepository) {

    suspend fun execute(pqrds: Pqrds): Result<String> =
        requestRepository.sendRequest(pqrds)


}