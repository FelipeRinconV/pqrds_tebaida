package com.example.apppqrs.utils

import android.content.ContentUris
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.util.Patterns
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.apppqrs.R
import com.example.apppqrs.data.Constant
import com.google.android.material.textfield.TextInputLayout
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import java.io.File
import java.lang.reflect.Type

object Utils {


    fun validateField(linearLayout: LinearLayout, context: Context): Boolean {
        var result = true
        var firstEditText: EditText? = null
        for (i in 0..linearLayout.childCount) {
            when (val view = linearLayout.getChildAt(i)) {
                is TextInputLayout -> {
                    val editText = view.editText as EditText
                    when {
                        editText.text.toString().isEmpty() -> {
                            if (firstEditText == null) firstEditText = editText
                            editText.error =
                                context.resources.getString(R.string.error_message_mandatory_field)
                            result = false
                        }
                        editText.inputType == Constant.TEXT_EMAIL_ADDRESS && !isEmail(editText.text.toString()) -> {
                            if (firstEditText == null) firstEditText = editText
                            editText.error =
                                context.resources.getString(R.string.error_message_wrong_email)
                            result = false
                        }
                    }
                }
                is RadioGroup -> {
                    val listRadioButton: MutableList<RadioButton> = mutableListOf()
                    for (child in 0..view.childCount) {
                        val viewRadioGroup = view.getChildAt(child)
                        if (viewRadioGroup is RadioButton) {
                            listRadioButton.add(viewRadioGroup)
                        }
                    }
                    val checkedList = listRadioButton.filter { it.isChecked };
                    if (checkedList.isEmpty())
                        result = false

                    listRadioButton.forEach {
                        it.error =
                            if (checkedList.isEmpty()) context.resources.getString(R.string.error_message_mandatory_field) else null
                    }
                }
            }
        }
        firstEditText?.requestFocus()
        return result
    }


    fun isEmail(email: String): Boolean {

        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }
}